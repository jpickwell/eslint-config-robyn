# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [4.1.2](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v4.1.1...v4.1.2) (2022-06-20)


### Bug Fixes

* ts consistent-type-definitions should be interface ([52dd275](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/52dd275fa5b76c3aed6e985a1ef368d171b5ce62))

### [4.1.1](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v4.1.0...v4.1.1) (2022-06-20)


### Bug Fixes

* import/extensions rule requires a ts override ([5bce6b3](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/5bce6b32404eb625002a5cef6c5543f625336241))

## [4.1.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v4.0.1...v4.1.0) (2022-06-17)


### Features

* remove momentjs plugin and config ([978462c](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/978462c08b1af542a92efc28dc35aa9a32ca2fa0))

### [4.0.1](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v4.0.0...v4.0.1) (2022-06-17)


### Bug Fixes

* update pnpm lock file ([bd65df3](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/bd65df3e4f3165d06dbf5178c3e4862402119bd4))

## [4.0.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.4.0...v4.0.0) (2022-06-17)


### ⚠ BREAKING CHANGES

* redo eslint config
* rename all .js files to .cjs

### Features

* redo eslint config ([1d9c6b8](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/1d9c6b88247820bf63c5d15ff92bb494a7d95b23))
* rename all .js files to .cjs ([7649ce1](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/7649ce19af42b37da000e398b4e0ea6ff3eb7bbb))
* update vue config ([57a6a56](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/57a6a569a127ef6588094ef81273812710ced728))


### Bug Fixes

* use functions for shared configs ([4d6b553](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/4d6b553ae9b0c0112e15831820bc95701c0b68c0))

## [3.4.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.3.0...v3.4.0) (2022-05-27)


### Features

* remove vue-scoped-css plugin ([aa2e699](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/aa2e69923ca19ede629f58d3aec6027d250dacf3))

## [3.3.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.2.3...v3.3.0) (2022-05-27)


### Features

* update typescript rules ([7474e90](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/7474e907ac987ddcced2537a70b7ef5bbbbcb057))

### [3.2.3](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.2.2...v3.2.3) (2022-05-24)

### [3.2.2](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.2.1...v3.2.2) (2022-05-23)

### [3.2.1](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.2.0...v3.2.1) (2022-05-23)

## [3.2.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.1.0...v3.2.0) (2022-05-23)


### Features

* move ts type-aware rules to separate config ([1224219](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/1224219f64041abdfb89124668a275cf1233e877))

## [3.1.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.0.2...v3.1.0) (2022-05-23)


### Features

* update dependencies ([c048e0b](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/c048e0bb5d89a60d0d4255a04a5d21b592daf59a))

### [3.0.2](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.0.1...v3.0.2) (2022-05-23)

### [3.0.1](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v3.0.0...v3.0.1) (2022-05-23)

## [3.0.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v2.1.3...v3.0.0) (2022-05-23)


### ⚠ BREAKING CHANGES

* Formatting settings and dependencies have been updated/modified.

### Features

* update rules and style ([076f024](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/076f02497ab8d3aab6fa3173d9fec341b7d0006c))


### Bug Fixes

* exclude typescript files from prettier xml plugin ([0ee1c7c](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/0ee1c7cb73d674187291ecbb7388edfaf8950b18))

### [2.1.3](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v2.1.2...v2.1.3) (2021-11-04)


### Bug Fixes

* handle git repo dependency detection ([323e0c4](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/323e0c498cce1bbde2c05c96a4039f79ab783ffc))

### [2.1.2](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v2.1.1...v2.1.2) (2021-11-04)


### Bug Fixes

* make husky install only work in dev ([cca561b](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/cca561b8392c562bd2f5f81e43c074322dc6f78d))

### [2.1.1](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v2.1.0...v2.1.1) (2021-11-04)


### Bug Fixes

* move husky install to postinstall ([d215dd4](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/d215dd459f4d0140a19030f4165c161c1ee5077c))

## [2.1.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v2.0.3...v2.1.0) (2021-11-03)


### Features

* add artisan file name to php override ([c761ac5](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/c761ac5254a490e004dabb6951fcc607d2167f84))

### [2.0.3](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v2.0.2...v2.0.3) (2021-11-03)

### [2.0.2](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v2.0.1...v2.0.2) (2021-07-13)


### Bug Fixes

* add lint-staged back to dev dependencies ([4c309ea](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/4c309ea28d35d6c6ed9c06ae57f1b1cd0d08f621))

### [2.0.1](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v2.0.0...v2.0.1) (2021-07-13)


### Bug Fixes

* husky hooks ([0bd1656](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/0bd1656d14bb4b7b3976d2b517682060774abe27))

## [2.0.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v1.3.0...v2.0.0) (2021-07-13)


### ⚠ BREAKING CHANGES

* update config and dependencies

### Features

* update config and dependencies ([2f3ef3a](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/2f3ef3aa5eec456bfec436dc65e31cb7d81f717b))

## [1.3.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v1.2.0...v1.3.0) (2020-09-02)

### Features

- update dependencies
  ([8c4a81b](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/8c4a81bbfb8cd37ac24d76b77b10e396b38346c5))

## [1.2.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v1.1.0...v1.2.0) (2020-07-13)

### Features

- improve prettier config
  ([7d14922](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/7d14922bf2d4e47250edd01bde7c6ad4eadb1c1f))

### Bug Fixes

- install pnpm in CI environment
  ([f1f6df3](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/f1f6df3338bc0549d4ed5f5ed9e6ea657e5cf8fb))
- pnpm installation
  ([359a210](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/359a2105d6482188a840d396057caad363660bb3))

## [1.1.0](https://gitlab.com/jpickwell/eslint-config-robyn/-/compare/v1.0.0...v1.1.0) (2020-05-21)

### Features

- add additional prettier plugins
  ([fd1a8e7](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/fd1a8e756e11539835620610e3ea6475b2b0de20))
- add eslint-plugin-markdown
  ([#1](https://gitlab.com/jpickwell/eslint-config-robyn/-/issues/1))
  ([f2b1708](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/f2b1708201d2159e21fd587795a201c4f073f89d))
- add stylelint config
  ([b97c0b4](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/b97c0b421705dda5b8bbed3f13d931ab65de3dbe))
- include additional Prettier plugins
  ([6854382](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/685438241e4b141381b350dc3d0ce0dacf2d18e7))

### Bug Fixes

- copy standard configs instead of referencing
  ([5023fc5](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/5023fc55898d1f98f8ddb3b8e921a090dc0bf42b))
- ESLint config validation issue
  ([e9c2b4d](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/e9c2b4d792cb5b72ae6aae225c5cc4de89135fa9))
- remove broken prettier-plugin-sh
  ([da1d9e0](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/da1d9e0211325f52f23df3033844117a8603b01c))
- remove preinstall script
  ([4a060f1](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/4a060f1cf85f0ce77ede439b3682751e0f145c6d))
- switch back to yarn
  ([761f10e](https://gitlab.com/jpickwell/eslint-config-robyn/-/commit/761f10ee3bf7e6d3a8953cd7143ce02c32acb5e3))

## 1.0.0 (2019-10-07)
