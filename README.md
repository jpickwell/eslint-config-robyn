# Robyn Style - Shareable Configs

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](CODE_OF_CONDUCT.md)
[![pipeline status](https://gitlab.com/jpickwell/eslint-config-robyn/badges/main/pipeline.svg)](https://gitlab.com/jpickwell/eslint-config-robyn/commits/main)
[![coverage report](https://gitlab.com/jpickwell/eslint-config-robyn/badges/main/coverage.svg)](https://gitlab.com/jpickwell/eslint-config-robyn/commits/main)

Shareable ESLint, Prettier, and Stylelint configs.

## IE Support

Turn off `unicorn/prefer-node-append` because IE doesn't support
`ParentNode.prototype.append`.

## Semantic Versioning Policy

## Changelog

## Contributing
