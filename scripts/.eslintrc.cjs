'use strict';

/** @typedef {import('eslint').BaseConfig} */

/** @type {BaseConfig} */
module.exports = {
	rules: {
		'no-console': `off`,

		// eslint-disable-next-line sort-keys
		'n/shebang': `off`,
	},
};
